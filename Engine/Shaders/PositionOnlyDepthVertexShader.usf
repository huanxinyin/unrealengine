// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

/*=============================================================================
	PositionOnlyDepthOnlyVertexShader.hlsl: Depth-only vertex shader.
=============================================================================*/

#include "Common.usf"
#include "Material.usf"
#include "VertexFactory.usf"

#if INSTANCED_STEREO
bool bNeedsInstancedStereoBias;
#endif

void Main(
	FPositionOnlyVertexFactoryInput Input,
	out float4 OutPosition : SV_POSITION
#if USE_GLOBAL_CLIP_PLANE
	, out float OutGlobalClipPlaneDistance : SV_ClipDistance
#endif
#if INSTANCED_STEREO
	, uint InstanceId : SV_InstanceID
	, out float OutClipDistance : SV_ClipDistance1
	, out float OutCullDistance : SV_CullDistance1
#endif
	)
{
#if INSTANCED_STEREO
	OutCullDistance = OutClipDistance = 0.0;
	const uint EyeIndex = VertexFactoryGetEyeIndex(InstanceId);
	ResolvedView = ResolveView(EyeIndex);
#else
	ResolvedView = ResolveView();
#endif

	float4 WorldPos = VertexFactoryGetWorldPosition(Input);
	ISOLATE
	{
		OutPosition = mul(WorldPos, ResolvedView.TranslatedWorldToClip);

		#if INSTANCED_STEREO
		BRANCH 
		if (bIsInstancedStereo)  
		{
			// Clip at the center of the screen
			OutCullDistance.x = OutClipDistance.x = dot(OutPosition, EyeClipEdge[EyeIndex]);

			// Scale to the width of a single eye viewport
			OutPosition.x *= 0.5 * Frame.HMDEyePaddingOffset;

			// Shift to the eye viewport
			OutPosition.x += (EyeOffsetScale[EyeIndex] * OutPosition.w) * (1.0f - 0.5 * Frame.HMDEyePaddingOffset);
		}
		
		// Bias the depth	
		OutPosition.z += InstancedStereoDepthBias * OutPosition.w * (float)bNeedsInstancedStereoBias;
		#endif
	}

#if USE_GLOBAL_CLIP_PLANE
	OutGlobalClipPlaneDistance = dot(ResolvedView.GlobalClippingPlane, float4(WorldPos.xyz - ResolvedView.PreViewTranslation.xyz, 1));
#endif
}
