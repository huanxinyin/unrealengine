﻿<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>Leap Motion</Name>
  <Location>Engine/Binaries/ThirdParty/LeapMotion/, Engine/Source/ThirdParty/Leap/, Engine/Plugins/Runtime/LeapMotion/</Location>
  <Date>2016-03-24T18:03:28.0415081-04:00</Date>
  <Function>Integration with the Leap Motion controller hardware for UE4</Function>
  <Justification>To support the Leap Motion platform</Justification>
  <Platforms>
    <Platform>PC</Platform>
    <Platform>Mac</Platform>
  </Platforms>
  <Products>
    <Product>UDK4</Product>
    <Product>UE4</Product>
  </Products>
  <TpsType>dll</TpsType>
  <Eula>Custom License Agreement in Contraxx</Eula>
  <RedistributeTo>
    <EndUserGroup>Licensees</EndUserGroup>
    <EndUserGroup>Git</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
  </RedistributeTo>
  <Redistribute>false</Redistribute>
  <IsSourceAvailable>false</IsSourceAvailable>
  <NoticeType>None</NoticeType>
  <Notification />
  <LicenseFolder />
</TpsData>