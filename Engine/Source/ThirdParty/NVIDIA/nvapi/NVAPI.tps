﻿<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>NVAPI</Name>
  <Location>Engine/Source/ThirdParty/NVIDIA/nvapi/</Location>
  <Date>2016-03-22T18:16:14.453578-04:00</Date>
  <Function>Allows the display of 3D images from Nvidia graphics cards on capable monitors</Function>
  <Justification />
  <Platforms>
    <Platform>PC</Platform>
  </Platforms>
  <Products>
    <Product>UDK</Product>
    <Product>UE3</Product>
    <Product>UDK4</Product>
    <Product>UE4</Product>
  </Products>
  <TpsType>Source Code</TpsType>
  <Eula>http://developer.nvidia.com/object/nvapi.html</Eula>
  <RedistributeTo>
    <EndUserGroup>Licensees</EndUserGroup>
    <EndUserGroup>Git</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
  </RedistributeTo>
  <Redistribute>false</Redistribute>
  <IsSourceAvailable>false</IsSourceAvailable>
  <NoticeType>Full EULA Text</NoticeType>
  <Notification />
  <LicenseFolder />
</TpsData>