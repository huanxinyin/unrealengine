﻿<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>Valve Steamworks SDK</Name>
  <Location>//UE4/Main/Engine/Source/ThirdParty/Steamworks/...</Location>
  <Date>2016-03-08T13:47:50.7845656-05:00</Date>
  <Function>SDK for connecting with the Steamworks platform</Function>
  <Justification>support licensees that want to use Steam with our engine</Justification>
  <Platforms />
  <Products />
  <TpsType>Source Code</TpsType>
  <Eula>http://store.steampowered.com/subscriber_agreement/</Eula>
  <RedistributeTo>
    <EndUserGroup>Licensees</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
  </RedistributeTo>
  <Redistribute>false</Redistribute>
  <IsSourceAvailable>true</IsSourceAvailable>
  <NoticeType>None</NoticeType>
  <Notification />
  <LicenseFolder>Steamworks.tps</LicenseFolder>
</TpsData>