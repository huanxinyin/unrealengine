﻿<?xml version="1.0" encoding="utf-8"?>
<TpsData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Name>UUIDs and GUIDs Memo</Name>
  <Location>/Engine/Source/Runtime/Core/Private/Misc/GuidGenerator</Location>
  <Date>2016-04-04T18:01:03.2456634-04:00</Date>
  <Function>GUID generator</Function>
  <Justification />
  <Platforms>
    <Platform>PC</Platform>
    <Platform>Mac</Platform>
  </Platforms>
  <Products>
    <Product>UDK4</Product>
    <Product>UE4</Product>
  </Products>
  <TpsType>Source Code</TpsType>
  <Eula />
  <RedistributeTo>
    <EndUserGroup>Licensees</EndUserGroup>
    <EndUserGroup>Git</EndUserGroup>
    <EndUserGroup>P4</EndUserGroup>
  </RedistributeTo>
  <Redistribute>false</Redistribute>
  <IsSourceAvailable>false</IsSourceAvailable>
  <NoticeType>None</NoticeType>
  <Notification />
  <LicenseFolder />
</TpsData>