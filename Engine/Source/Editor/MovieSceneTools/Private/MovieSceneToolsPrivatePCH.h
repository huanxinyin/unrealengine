// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#pragma once


#include "UnrealEd.h"
#include "MovieScene.h"
#include "ScopedTransaction.h"
#include "ISequencerSection.h"
#include "ISectionLayoutBuilder.h"
#include "KeyParams.h"
#include "MovieSceneSequenceInstance.h"
#include "MovieSceneTrackEditor.h"
#include "IMovieSceneTools.h"
#include "MovieSceneSection.h"
#include "PropertySection.h"
#include "PropertyTrackEditor.h"
#include "CommonMovieSceneTools.h"
#include "MovieSceneToolHelpers.h"
#include "MovieSceneCommonHelpers.h"
#include "MovieSceneToolsProjectSettings.h"

#include "ThumbnailSection.h"
#include "CameraCutSection.h"
#include "CinematicShotSection.h"

#include "TrackEditorThumbnail.h"
#include "TrackEditorThumbnailPool.h"
