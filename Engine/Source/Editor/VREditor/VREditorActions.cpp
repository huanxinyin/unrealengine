// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "VREditorModule.h"
#include "VREditorActions.h"

#define LOCTEXT_NAMESPACE "VREditorActions"

void FVREditorActions::RegisterCommands()
{
	// @todo VREditor: Register any commands we have here
	// UI_COMMAND(DoSomething, "Do Something", "Did something tooltip.", EUserInterfaceActionType::Button, FInputChord());
}

#undef LOCTEXT_NAMESPACE